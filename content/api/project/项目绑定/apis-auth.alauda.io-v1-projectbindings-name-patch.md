+++
title = "差异化更新项目和集群的绑定关系信息"
description = "/apis/auth.alauda.io/v1/projectbindings/{name} PATCH"
weight = 4
path = "PATCH /apis/auth.alauda.io/v1/projectbindings/{name}"
+++


{{%api path="/apis/auth.alauda.io/v1/projectbindings/{name}" verb="PATCH" %}}
