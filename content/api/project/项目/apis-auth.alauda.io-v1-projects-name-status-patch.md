+++
title = "差异化更新项目的状态"
description = "/apis/auth.alauda.io/v1/projects/{name}/status PATCH"
weight = 7
path = "PATCH /apis/auth.alauda.io/v1/projects/{name}/status"
+++


{{%api path="/apis/auth.alauda.io/v1/projects/{name}/status" verb="PATCH" %}}
