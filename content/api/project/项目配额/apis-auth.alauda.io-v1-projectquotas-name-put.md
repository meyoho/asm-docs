+++
title = "更新项目配额"
description = "/apis/auth.alauda.io/v1/projectquotas/{name} PUT"
weight = 6
path = "PUT /apis/auth.alauda.io/v1/projectquotas/{name}"
+++


{{%api path="/apis/auth.alauda.io/v1/projectquotas/{name}" verb="PUT" %}}
