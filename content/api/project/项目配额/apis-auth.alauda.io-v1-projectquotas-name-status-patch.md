+++
title = "差异化更新项目配额的状态"
description = "/apis/auth.alauda.io/v1/projectquotas/{name}/status PATCH"
weight = 7
path = "PATCH /apis/auth.alauda.io/v1/projectquotas/{name}/status"
+++


{{%api path="/apis/auth.alauda.io/v1/projectquotas/{name}/status" verb="PATCH" %}}
