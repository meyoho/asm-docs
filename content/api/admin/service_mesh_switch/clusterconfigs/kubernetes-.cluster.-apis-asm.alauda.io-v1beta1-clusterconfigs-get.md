+++
title = "获取 ClusterConfig 列表"
description = "获取 ClusterConfig 列表"
weight = 10000
path = "GET /kubernetes/.cluster./apis/asm.alauda.io/v1beta1/clusterconfigs"
+++


{{%api path="/kubernetes/.cluster./apis/asm.alauda.io/v1beta1/clusterconfigs" verb="GET" %}}
