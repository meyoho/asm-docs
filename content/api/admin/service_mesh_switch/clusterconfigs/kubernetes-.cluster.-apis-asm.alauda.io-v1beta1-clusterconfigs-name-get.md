+++
title = "查看指定的 ClusterConfig 详情"
description = "查看指定的 ClusterConfig 详情"
weight = 10000
path = "GET /kubernetes/.cluster./apis/asm.alauda.io/v1beta1/clusterconfigs/{name}"
+++


{{%api path="/kubernetes/.cluster./apis/asm.alauda.io/v1beta1/clusterconfigs/{name}" verb="GET" %}}
