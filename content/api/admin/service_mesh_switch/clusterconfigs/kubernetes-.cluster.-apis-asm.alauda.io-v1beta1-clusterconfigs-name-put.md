+++
title = "更新 ClusterConfig"
description = "更新 ClusterConfig"
weight = 10000
path = "PUT /kubernetes/.cluster./apis/asm.alauda.io/v1beta1/clusterconfigs/{name}"
+++


{{%api path="/kubernetes/.cluster./apis/asm.alauda.io/v1beta1/clusterconfigs/{name}" verb="PUT" %}}
