+++
title = "批量删除 ClusterConfig"
description = "批量删除 ClusterConfig"
weight = 10000
path = "DELETE /kubernetes/.cluster./apis/asm.alauda.io/v1beta1/clusterconfigs"
+++


{{%api path="/kubernetes/.cluster./apis/asm.alauda.io/v1beta1/clusterconfigs" verb="DELETE" %}}
