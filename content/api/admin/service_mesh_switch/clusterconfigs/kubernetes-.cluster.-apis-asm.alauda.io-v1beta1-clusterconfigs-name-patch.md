+++
title = "差异化更新 ClusterConfig"
description = "差异化更新 ClusterConfig"
weight = 10000
path = "PATCH /kubernetes/.cluster./apis/asm.alauda.io/v1beta1/clusterconfigs/{name}"
+++


{{%api path="/kubernetes/.cluster./apis/asm.alauda.io/v1beta1/clusterconfigs/{name}" verb="PATCH" %}}
