+++
title = "删除指定的 ClusterConfig"
description = "删除指定的 ClusterConfig"
weight = 10000
path = "DELETE /kubernetes/.cluster./apis/asm.alauda.io/v1beta1/clusterconfigs/{name}"
+++


{{%api path="/kubernetes/.cluster./apis/asm.alauda.io/v1beta1/clusterconfigs/{name}" verb="DELETE" %}}
