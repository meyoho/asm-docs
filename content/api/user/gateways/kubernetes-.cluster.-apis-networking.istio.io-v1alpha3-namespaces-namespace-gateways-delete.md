+++
title = "批量删除 Gateway"
description = "批量删除 Gateway"
weight = 10000
path = "DELETE /kubernetes/.cluster./apis/networking.istio.io/v1alpha3/namespaces/{namespace}/gateways"
+++


{{%api path="/kubernetes/.cluster./apis/networking.istio.io/v1alpha3/namespaces/{namespace}/gateways" verb="DELETE" %}}
