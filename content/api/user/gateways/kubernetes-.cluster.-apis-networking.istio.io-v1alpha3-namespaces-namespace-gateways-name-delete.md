+++
title = "删除指定 Gateway"
description = "删除指定 Gateway"
weight = 10000
path = "DELETE /kubernetes/.cluster./apis/networking.istio.io/v1alpha3/namespaces/{namespace}/gateways/{name}"
+++


{{%api path="/kubernetes/.cluster./apis/networking.istio.io/v1alpha3/namespaces/{namespace}/gateways/{name}" verb="DELETE" %}}
