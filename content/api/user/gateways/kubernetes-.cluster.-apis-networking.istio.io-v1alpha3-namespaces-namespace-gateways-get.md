+++
title = "查看 Gateway 列表"
description = "查看 Gateway 列表"
weight = 10000
path = "GET /kubernetes/.cluster./apis/networking.istio.io/v1alpha3/namespaces/{namespace}/gateways"
+++


{{%api path="/kubernetes/.cluster./apis/networking.istio.io/v1alpha3/namespaces/{namespace}/gateways" verb="GET" %}}
