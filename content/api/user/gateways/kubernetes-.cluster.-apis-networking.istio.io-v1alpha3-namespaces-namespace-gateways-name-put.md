+++
title = "更新 Gateway"
description = "更新 Gateway"
weight = 10000
path = "PUT /kubernetes/.cluster./apis/networking.istio.io/v1alpha3/namespaces/{namespace}/gateways/{name}"
+++


{{%api path="/kubernetes/.cluster./apis/networking.istio.io/v1alpha3/namespaces/{namespace}/gateways/{name}" verb="PUT" %}}
