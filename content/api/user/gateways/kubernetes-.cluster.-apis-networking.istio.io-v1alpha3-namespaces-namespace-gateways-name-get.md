+++
title = "查看指定 Gateway"
description = "查看指定 Gateway"
weight = 10000
path = "GET /kubernetes/.cluster./apis/networking.istio.io/v1alpha3/namespaces/{namespace}/gateways/{name}"
+++


{{%api path="/kubernetes/.cluster./apis/networking.istio.io/v1alpha3/namespaces/{namespace}/gateways/{name}" verb="GET" %}}
