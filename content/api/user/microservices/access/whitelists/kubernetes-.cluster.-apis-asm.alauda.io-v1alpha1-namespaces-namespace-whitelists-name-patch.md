+++
title = "差异化更新 WhiteList"
description = "差异化更新 WhiteList"
weight = 10000
path = "PATCH /kubernetes/.cluster./apis/asm.alauda.io/v1alpha1/namespaces/{namespace}/whitelists/{name}"
+++


{{%api path="/kubernetes/.cluster./apis/asm.alauda.io/v1alpha1/namespaces/{namespace}/whitelists/{name}" verb="PATCH" %}}
