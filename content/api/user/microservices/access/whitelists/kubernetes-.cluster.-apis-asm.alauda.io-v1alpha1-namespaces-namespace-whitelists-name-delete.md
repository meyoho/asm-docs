+++
title = "删除指定 WhiteList"
description = "删除指定 WhiteList"
weight = 10000
path = "DELETE /kubernetes/.cluster./apis/asm.alauda.io/v1alpha1/namespaces/{namespace}/whitelists/{name}"
+++


{{%api path="/kubernetes/.cluster./apis/asm.alauda.io/v1alpha1/namespaces/{namespace}/whitelists/{name}" verb="DELETE" %}}
