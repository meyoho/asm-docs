+++
title = "更新 WhiteList"
description = "更新 WhiteList"
weight = 10000
path = "PUT /kubernetes/.cluster./apis/asm.alauda.io/v1alpha1/namespaces/{namespace}/whitelists/{name}"
+++


{{%api path="/kubernetes/.cluster./apis/asm.alauda.io/v1alpha1/namespaces/{namespace}/whitelists/{name}" verb="PUT" %}}
