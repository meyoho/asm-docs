+++
title = "查看 WhiteList 列表"
description = "查看 WhiteList 列表"
weight = 10000
path = "GET /kubernetes/.cluster./apis/asm.alauda.io/v1alpha1/namespaces/{namespace}/whitelists"
+++


{{%api path="/kubernetes/.cluster./apis/asm.alauda.io/v1alpha1/namespaces/{namespace}/whitelists" verb="GET" %}}
