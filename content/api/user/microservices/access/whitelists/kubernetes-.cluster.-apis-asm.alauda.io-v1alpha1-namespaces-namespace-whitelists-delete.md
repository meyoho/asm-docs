+++
title = "批量删除 WhiteList"
description = "批量删除 WhiteList"
weight = 10000
path = "DELETE /kubernetes/.cluster./apis/asm.alauda.io/v1alpha1/namespaces/{namespace}/whitelists"
+++


{{%api path="/kubernetes/.cluster./apis/asm.alauda.io/v1alpha1/namespaces/{namespace}/whitelists" verb="DELETE" %}}
