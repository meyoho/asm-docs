+++
title = "创建 VirtualService"
description = "创建 VirtualService"
weight = 10000
path = "POST /kubernetes/.cluster./apis/networking.istio.io/v1alpha3/namespaces/{namespace}/virtualservices"
+++


{{%api path="/kubernetes/.cluster./apis/networking.istio.io/v1alpha3/namespaces/{namespace}/virtualservices" verb="POST" %}}
