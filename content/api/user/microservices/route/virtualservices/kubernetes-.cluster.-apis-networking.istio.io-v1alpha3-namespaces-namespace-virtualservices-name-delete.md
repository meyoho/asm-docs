+++
title = "删除指定 VirtualService"
description = "删除指定 VirtualService"
weight = 10000
path = "DELETE /kubernetes/.cluster./apis/networking.istio.io/v1alpha3/namespaces/{namespace}/virtualservices/{name}"
+++


{{%api path="/kubernetes/.cluster./apis/networking.istio.io/v1alpha3/namespaces/{namespace}/virtualservices/{name}" verb="DELETE" %}}
