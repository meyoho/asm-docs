+++
title = "查看 VirtualService 列表"
description = "查看 VirtualService 列表"
weight = 10000
path = "GET /kubernetes/.cluster./apis/networking.istio.io/v1alpha3/namespaces/{namespace}/virtualservices"
+++


{{%api path="/kubernetes/.cluster./apis/networking.istio.io/v1alpha3/namespaces/{namespace}/virtualservices" verb="GET" %}}
