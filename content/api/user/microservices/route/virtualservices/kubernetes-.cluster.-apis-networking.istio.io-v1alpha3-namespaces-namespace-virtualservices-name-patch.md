+++
title = "差异化更新 VirtualService"
description = "差异化更新 VirtualService"
weight = 10000
path = "PATCH /kubernetes/.cluster./apis/networking.istio.io/v1alpha3/namespaces/{namespace}/virtualservices/{name}"
+++


{{%api path="/kubernetes/.cluster./apis/networking.istio.io/v1alpha3/namespaces/{namespace}/virtualservices/{name}" verb="PATCH" %}}
