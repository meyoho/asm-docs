+++
title = "查看指定 VirtualService"
description = "查看指定 VirtualService"
weight = 10000
path = "GET /kubernetes/.cluster./apis/networking.istio.io/v1alpha3/namespaces/{namespace}/virtualservices/{name}"
+++


{{%api path="/kubernetes/.cluster./apis/networking.istio.io/v1alpha3/namespaces/{namespace}/virtualservices/{name}" verb="GET" %}}
