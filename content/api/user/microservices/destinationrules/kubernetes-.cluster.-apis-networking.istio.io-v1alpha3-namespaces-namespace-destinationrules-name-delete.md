+++
title = "删除指定的 DestinationRule"
description = "删除指定的 DestinationRule"
weight = 10000
path = "DELETE /kubernetes/.cluster./apis/networking.istio.io/v1alpha3/namespaces/{namespace}/destinationrules/{name}"
+++


{{%api path="/kubernetes/.cluster./apis/networking.istio.io/v1alpha3/namespaces/{namespace}/destinationrules/{name}" verb="DELETE" %}}
