+++
title = "批量删除 DestinationRule"
description = "批量删除 DestinationRule"
weight = 10000
path = "DELETE /kubernetes/.cluster./apis/networking.istio.io/v1alpha3/namespaces/{namespace}/destinationrules"
+++


{{%api path="/kubernetes/.cluster./apis/networking.istio.io/v1alpha3/namespaces/{namespace}/destinationrules" verb="DELETE" %}}
