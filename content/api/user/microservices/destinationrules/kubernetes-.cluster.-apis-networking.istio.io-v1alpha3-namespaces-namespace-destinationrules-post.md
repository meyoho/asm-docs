+++
title = "创建 DestinationRule"
description = "创建 DestinationRule"
weight = 10000
path = "POST /kubernetes/.cluster./apis/networking.istio.io/v1alpha3/namespaces/{namespace}/destinationrules"
+++


{{%api path="/kubernetes/.cluster./apis/networking.istio.io/v1alpha3/namespaces/{namespace}/destinationrules" verb="POST" %}}
