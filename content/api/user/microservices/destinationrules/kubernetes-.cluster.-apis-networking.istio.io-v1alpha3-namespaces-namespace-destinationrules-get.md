+++
title = "获取 DestinationRule 列表"
description = "获取 DestinationRule 列表"
weight = 10000
path = "GET /kubernetes/.cluster./apis/networking.istio.io/v1alpha3/namespaces/{namespace}/destinationrules"
+++


{{%api path="/kubernetes/.cluster./apis/networking.istio.io/v1alpha3/namespaces/{namespace}/destinationrules" verb="GET" %}}
