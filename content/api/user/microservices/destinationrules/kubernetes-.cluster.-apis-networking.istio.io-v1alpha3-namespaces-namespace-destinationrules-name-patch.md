+++
title = "差异化更新 DestinationRule"
description = "差异化更新 DestinationRule"
weight = 10000
path = "PATCH /kubernetes/.cluster./apis/networking.istio.io/v1alpha3/namespaces/{namespace}/destinationrules/{name}"
+++


{{%api path="/kubernetes/.cluster./apis/networking.istio.io/v1alpha3/namespaces/{namespace}/destinationrules/{name}" verb="PATCH" %}}
