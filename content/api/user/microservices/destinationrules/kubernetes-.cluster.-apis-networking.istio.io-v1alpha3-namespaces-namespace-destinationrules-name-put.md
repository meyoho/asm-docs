+++
title = "更新 DestinationRule"
description = "更新 DestinationRule"
weight = 10000
path = "PUT /kubernetes/.cluster./apis/networking.istio.io/v1alpha3/namespaces/{namespace}/destinationrules/{name}"
+++


{{%api path="/kubernetes/.cluster./apis/networking.istio.io/v1alpha3/namespaces/{namespace}/destinationrules/{name}" verb="PUT" %}}
