+++
title = "批量删除 OutlierDetection"
description = "批量删除 OutlierDetection"
weight = 10000
path = "DELETE /kubernetes/.cluster./apis/asm.alauda.io/v1alpha1/namespaces/{namespace}/outlierdetections"
+++


{{%api path="/kubernetes/.cluster./apis/asm.alauda.io/v1alpha1/namespaces/{namespace}/outlierdetections" verb="DELETE" %}}
