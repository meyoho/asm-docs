+++
title = "更新 OutlierDetection"
description = "更新 OutlierDetection"
weight = 10000
path = "PUT /kubernetes/.cluster./apis/asm.alauda.io/v1alpha1/namespaces/{namespace}/outlierdetections/{name}"
+++


{{%api path="/kubernetes/.cluster./apis/asm.alauda.io/v1alpha1/namespaces/{namespace}/outlierdetections/{name}" verb="PUT" %}}
