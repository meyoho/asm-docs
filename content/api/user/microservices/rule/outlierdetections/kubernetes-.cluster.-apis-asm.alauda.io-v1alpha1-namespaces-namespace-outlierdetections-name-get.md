+++
title = "查看指定 OutlierDetection"
description = "查看指定 OutlierDetection"
weight = 10000
path = "GET /kubernetes/.cluster./apis/asm.alauda.io/v1alpha1/namespaces/{namespace}/outlierdetections/{name}"
+++


{{%api path="/kubernetes/.cluster./apis/asm.alauda.io/v1alpha1/namespaces/{namespace}/outlierdetections/{name}" verb="GET" %}}
