+++
title = "删除指定 OutlierDetection"
description = "/kubernetes/.cluster./apis/asm.alauda.io/v1alpha1/namespaces/{namespace}/outlierdetections/{name} DELETE"
weight = 10000
path = "DELETE /kubernetes/.cluster./apis/asm.alauda.io/v1alpha1/namespaces/{namespace}/outlierdetections/{name}"
+++


{{%api path="/kubernetes/.cluster./apis/asm.alauda.io/v1alpha1/namespaces/{namespace}/outlierdetections/{name}" verb="DELETE" %}}
