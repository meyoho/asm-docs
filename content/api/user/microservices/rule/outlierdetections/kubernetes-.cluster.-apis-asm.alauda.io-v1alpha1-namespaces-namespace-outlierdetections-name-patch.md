+++
title = "差异化更新 OutlierDetection"
description = "差异化更新 OutlierDetection"
weight = 10000
path = "PATCH /kubernetes/.cluster./apis/asm.alauda.io/v1alpha1/namespaces/{namespace}/outlierdetections/{name}"
+++


{{%api path="/kubernetes/.cluster./apis/asm.alauda.io/v1alpha1/namespaces/{namespace}/outlierdetections/{name}" verb="PATCH" %}}
