+++
title = "更新 LoadBalancer"
description = "更新 LoadBalancer"
weight = 10000
path = "PUT /kubernetes/.cluster./apis/asm.alauda.io/v1beta1/namespaces/{namespace}/loadbalancers/{name}"
+++


{{%api path="/kubernetes/.cluster./apis/asm.alauda.io/v1beta1/namespaces/{namespace}/loadbalancers/{name}" verb="PUT" %}}
