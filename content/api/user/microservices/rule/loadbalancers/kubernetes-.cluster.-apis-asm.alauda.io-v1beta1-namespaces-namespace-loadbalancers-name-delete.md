+++
title = "删除指定 LoadBalancer"
description = "删除指定 LoadBalancer"
weight = 10000
path = "DELETE /kubernetes/.cluster./apis/asm.alauda.io/v1beta1/namespaces/{namespace}/loadbalancers/{name}"
+++


{{%api path="/kubernetes/.cluster./apis/asm.alauda.io/v1beta1/namespaces/{namespace}/loadbalancers/{name}" verb="DELETE" %}}
