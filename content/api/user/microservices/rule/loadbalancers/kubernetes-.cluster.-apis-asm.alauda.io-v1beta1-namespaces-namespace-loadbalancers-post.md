+++
title = "创建 LoadBalancer"
description = "创建 LoadBalancer"
weight = 10000
path = "POST /kubernetes/.cluster./apis/asm.alauda.io/v1beta1/namespaces/{namespace}/loadbalancers"
+++


{{%api path="/kubernetes/.cluster./apis/asm.alauda.io/v1beta1/namespaces/{namespace}/loadbalancers" verb="POST" %}}
