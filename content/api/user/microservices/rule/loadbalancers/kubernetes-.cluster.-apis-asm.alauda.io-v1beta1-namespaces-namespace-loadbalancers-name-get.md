+++
title = "查看指定 LoadBalancer"
description = "查看指定 LoadBalancer"
weight = 10000
path = "GET /kubernetes/.cluster./apis/asm.alauda.io/v1beta1/namespaces/{namespace}/loadbalancers/{name}"
+++


{{%api path="/kubernetes/.cluster./apis/asm.alauda.io/v1beta1/namespaces/{namespace}/loadbalancers/{name}" verb="GET" %}}
