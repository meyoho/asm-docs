+++
title = "批量删除 LoadBalancer"
description = "批量删除 LoadBalancer"
weight = 10000
path = "DELETE /kubernetes/.cluster./apis/asm.alauda.io/v1beta1/namespaces/{namespace}/loadbalancers"
+++


{{%api path="/kubernetes/.cluster./apis/asm.alauda.io/v1beta1/namespaces/{namespace}/loadbalancers" verb="DELETE" %}}
