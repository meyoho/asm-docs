+++
title = "差异化更新 LoadBalancer"
description = "差异化更新 LoadBalancer"
weight = 10000
path = "PATCH /kubernetes/.cluster./apis/asm.alauda.io/v1beta1/namespaces/{namespace}/loadbalancers/{name}"
+++


{{%api path="/kubernetes/.cluster./apis/asm.alauda.io/v1beta1/namespaces/{namespace}/loadbalancers/{name}" verb="PATCH" %}}
