+++
title = "查看 LoadBalancer 列表"
description = "查看 LoadBalancer 列表"
weight = 10000
path = "GET /kubernetes/.cluster./apis/asm.alauda.io/v1beta1/namespaces/{namespace}/loadbalancers"
+++


{{%api path="/kubernetes/.cluster./apis/asm.alauda.io/v1beta1/namespaces/{namespace}/loadbalancers" verb="GET" %}}
