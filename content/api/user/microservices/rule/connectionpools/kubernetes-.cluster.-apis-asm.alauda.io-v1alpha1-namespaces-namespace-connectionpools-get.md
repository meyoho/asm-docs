+++
title = "查看 ConnectionPool 列表"
description = "查看 ConnectionPool 列表"
weight = 10000
path = "GET /kubernetes/.cluster./apis/asm.alauda.io/v1alpha1/namespaces/{namespace}/connectionpools"
+++


{{%api path="/kubernetes/.cluster./apis/asm.alauda.io/v1alpha1/namespaces/{namespace}/connectionpools" verb="GET" %}}
