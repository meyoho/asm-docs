+++
title = "差异化更新 ConnectionPool"
description = "差异化更新 ConnectionPool"
weight = 10000
path = "PATCH /kubernetes/.cluster./apis/asm.alauda.io/v1alpha1/namespaces/{namespace}/connectionpools/{name}"
+++


{{%api path="/kubernetes/.cluster./apis/asm.alauda.io/v1alpha1/namespaces/{namespace}/connectionpools/{name}" verb="PATCH" %}}
