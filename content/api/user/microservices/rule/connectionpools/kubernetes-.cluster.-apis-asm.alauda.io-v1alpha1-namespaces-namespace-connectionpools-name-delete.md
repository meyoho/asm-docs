+++
title = "删除 ConnectionPool"
description = "删除 ConnectionPool"
weight = 10000
path = "DELETE /kubernetes/.cluster./apis/asm.alauda.io/v1alpha1/namespaces/{namespace}/connectionpools/{name}"
+++


{{%api path="/kubernetes/.cluster./apis/asm.alauda.io/v1alpha1/namespaces/{namespace}/connectionpools/{name}" verb="DELETE" %}}
