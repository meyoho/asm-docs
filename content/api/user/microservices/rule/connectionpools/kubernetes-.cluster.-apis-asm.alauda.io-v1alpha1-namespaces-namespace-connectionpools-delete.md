+++
title = "批量删除 ConnectionPool"
description = "批量删除 ConnectionPool"
weight = 10000
path = "DELETE /kubernetes/.cluster./apis/asm.alauda.io/v1alpha1/namespaces/{namespace}/connectionpools"
+++


{{%api path="/kubernetes/.cluster./apis/asm.alauda.io/v1alpha1/namespaces/{namespace}/connectionpools" verb="DELETE" %}}
