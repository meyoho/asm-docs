+++
title = "更新 ConnectionPool"
description = "更新 ConnectionPool"
weight = 10000
path = "PUT /kubernetes/.cluster./apis/asm.alauda.io/v1alpha1/namespaces/{namespace}/connectionpools/{name}"
+++


{{%api path="/kubernetes/.cluster./apis/asm.alauda.io/v1alpha1/namespaces/{namespace}/connectionpools/{name}" verb="PUT" %}}
