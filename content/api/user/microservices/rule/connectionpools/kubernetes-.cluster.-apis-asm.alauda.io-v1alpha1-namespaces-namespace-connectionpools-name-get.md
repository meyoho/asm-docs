+++
title = "查看指定 ConnectionPool"
description = "查看指定 ConnectionPool"
weight = 10000
path = "GET /kubernetes/.cluster./apis/asm.alauda.io/v1alpha1/namespaces/{namespace}/connectionpools/{name}"
+++


{{%api path="/kubernetes/.cluster./apis/asm.alauda.io/v1alpha1/namespaces/{namespace}/connectionpools/{name}" verb="GET" %}}
