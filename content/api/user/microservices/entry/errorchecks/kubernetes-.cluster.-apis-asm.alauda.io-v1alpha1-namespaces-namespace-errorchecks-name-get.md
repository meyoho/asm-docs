+++
title = "查看指定的错误检查"
description = "/kubernetes/.cluster./apis/asm.alauda.io/v1alpha1/namespaces/{namespace}/errorchecks/{name} GET"
weight = 4
path = "GET /kubernetes/.cluster./apis/asm.alauda.io/v1alpha1/namespaces/{namespace}/errorchecks/{name}"
+++


{{%api path="/kubernetes/.cluster./apis/asm.alauda.io/v1alpha1/namespaces/{namespace}/errorchecks/{name}" verb="GET" %}}
