+++
title = "更新错误检查"
description = "/kubernetes/.cluster./apis/asm.alauda.io/v1alpha1/namespaces/{namespace}/errorchecks/{name} PUT"
weight = 2
path = "PUT /kubernetes/.cluster./apis/asm.alauda.io/v1alpha1/namespaces/{namespace}/errorchecks/{name}"
+++


{{%api path="/kubernetes/.cluster./apis/asm.alauda.io/v1alpha1/namespaces/{namespace}/errorchecks/{name}" verb="PUT" %}}
