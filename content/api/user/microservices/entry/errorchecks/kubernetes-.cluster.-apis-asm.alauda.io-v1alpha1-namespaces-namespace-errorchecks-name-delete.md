+++
title = "删除指定的错误检查"
description = "/kubernetes/.cluster./apis/asm.alauda.io/v1alpha1/namespaces/{namespace}/errorchecks/{name} DELETE"
weight = 6
path = "DELETE /kubernetes/.cluster./apis/asm.alauda.io/v1alpha1/namespaces/{namespace}/errorchecks/{name}"
+++


{{%api path="/kubernetes/.cluster./apis/asm.alauda.io/v1alpha1/namespaces/{namespace}/errorchecks/{name}" verb="DELETE" %}}
