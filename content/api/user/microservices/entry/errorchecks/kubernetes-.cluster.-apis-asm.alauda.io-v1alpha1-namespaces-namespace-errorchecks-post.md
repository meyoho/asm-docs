+++
title = "创建错误检查"
description = "/kubernetes/.cluster./apis/asm.alauda.io/v1alpha1/namespaces/{namespace}/errorchecks POST"
weight = 1
path = "POST /kubernetes/.cluster./apis/asm.alauda.io/v1alpha1/namespaces/{namespace}/errorchecks"
+++


{{%api path="/kubernetes/.cluster./apis/asm.alauda.io/v1alpha1/namespaces/{namespace}/errorchecks" verb="POST" %}}
