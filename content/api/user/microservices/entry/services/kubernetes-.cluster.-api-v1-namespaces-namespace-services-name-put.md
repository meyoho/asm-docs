+++
title = "更新 Service"
description = "更新 Service"
weight = 10000
path = "PUT /kubernetes/.cluster./api/v1/namespaces/{namespace}/services/{name}"
+++


{{%api path="/kubernetes/.cluster./api/v1/namespaces/{namespace}/services/{name}" verb="PUT" %}}
