+++
title = "删除指定 Service"
description = "删除指定 Service"
weight = 10000
path = "DELETE /kubernetes/.cluster./api/v1/namespaces/{namespace}/services/{name}"
+++


{{%api path="/kubernetes/.cluster./api/v1/namespaces/{namespace}/services/{name}" verb="DELETE" %}}
