+++
title = "查看 Service 状态"
description = "查看 Service 状态"
weight = 10000
path = "GET /kubernetes/.cluster./api/v1/namespaces/{namespace}/services/{name}/status"
+++


{{%api path="/kubernetes/.cluster./api/v1/namespaces/{namespace}/services/{name}/status" verb="GET" %}}
