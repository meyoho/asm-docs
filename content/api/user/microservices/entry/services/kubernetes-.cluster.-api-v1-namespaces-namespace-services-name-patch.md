+++
title = "差异化更新 Service"
description = "差异化更新 Service"
weight = 10000
path = "PATCH /kubernetes/.cluster./api/v1/namespaces/{namespace}/services/{name}"
+++


{{%api path="/kubernetes/.cluster./api/v1/namespaces/{namespace}/services/{name}" verb="PATCH" %}}
