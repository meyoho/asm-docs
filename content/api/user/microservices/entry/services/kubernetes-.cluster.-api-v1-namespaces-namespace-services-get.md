+++
title = "查看 Service 类型资源列表"
description = "查看 Service 类型资源列表"
weight = 10000
path = "GET /kubernetes/.cluster./api/v1/namespaces/{namespace}/services"
+++


{{%api path="/kubernetes/.cluster./api/v1/namespaces/{namespace}/services" verb="GET" %}}
