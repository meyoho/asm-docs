+++
title = "差异化更新 Service 状态"
description = "差异化更新 Service 状态"
weight = 10000
path = "PATCH /kubernetes/.cluster./api/v1/namespaces/{namespace}/services/{name}/status"
+++


{{%api path="/kubernetes/.cluster./api/v1/namespaces/{namespace}/services/{name}/status" verb="PATCH" %}}
