+++
title = "查看指定的 Service 详情"
description = "查看指定的 Service 详情"
weight = 10000
path = "GET /kubernetes/.cluster./api/v1/namespaces/{namespace}/services/{name}"
+++


{{%api path="/kubernetes/.cluster./api/v1/namespaces/{namespace}/services/{name}" verb="GET" %}}
