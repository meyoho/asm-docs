+++
title = "更换 Service 状态"
description = "更换 Service 状态"
weight = 10000
path = "PUT /kubernetes/.cluster./api/v1/namespaces/{namespace}/services/{name}/status"
+++


{{%api path="/kubernetes/.cluster./api/v1/namespaces/{namespace}/services/{name}/status" verb="PUT" %}}
