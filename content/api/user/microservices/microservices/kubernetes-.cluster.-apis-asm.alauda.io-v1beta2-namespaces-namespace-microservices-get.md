+++
title = "查看微服务列表"
weight = 10000
path = "查看微服务列表"
+++


{{%api path="/kubernetes/.cluster./apis/asm.alauda.io/v1beta2/namespaces/{namespace}/microservices" verb="GET" %}}
