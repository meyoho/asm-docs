+++
title = "删除指定的微服务"
description = "删除指定的微服务"
weight = 10000
path = "DELETE /kubernetes/.cluster./apis/asm.alauda.io/v1beta2/namespaces/{namespace}/microservices/{name}"
+++


{{%api path="/kubernetes/.cluster./apis/asm.alauda.io/v1beta2/namespaces/{namespace}/microservices/{name}" verb="DELETE" %}}
