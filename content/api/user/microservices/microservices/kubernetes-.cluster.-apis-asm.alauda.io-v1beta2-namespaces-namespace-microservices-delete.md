+++
title = "批量删除微服务"
description = "批量删除微服务"
weight = 10000
path = "DELETE /kubernetes/.cluster./apis/asm.alauda.io/v1beta2/namespaces/{namespace}/microservices"
+++


{{%api path="/kubernetes/.cluster./apis/asm.alauda.io/v1beta2/namespaces/{namespace}/microservices" verb="DELETE" %}}
