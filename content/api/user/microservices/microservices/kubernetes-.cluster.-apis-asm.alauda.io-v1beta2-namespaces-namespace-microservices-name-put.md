+++
title = "更新微服务"
description = "更新微服务"
weight = 10000
path = "PUT /kubernetes/.cluster./apis/asm.alauda.io/v1beta2/namespaces/{namespace}/microservices/{name}"
+++


{{%api path="/kubernetes/.cluster./apis/asm.alauda.io/v1beta2/namespaces/{namespace}/microservices/{name}" verb="PUT" %}}
