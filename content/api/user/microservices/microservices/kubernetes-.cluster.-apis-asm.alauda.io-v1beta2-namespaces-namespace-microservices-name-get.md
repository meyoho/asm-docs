+++
title = "查看指定的微服务详情"
description = "查看指定的微服务详情"
weight = 10000
path = "GET /kubernetes/.cluster./apis/asm.alauda.io/v1beta2/namespaces/{namespace}/microservices/{name}"
+++


{{%api path="/kubernetes/.cluster./apis/asm.alauda.io/v1beta2/namespaces/{namespace}/microservices/{name}" verb="GET" %}}
