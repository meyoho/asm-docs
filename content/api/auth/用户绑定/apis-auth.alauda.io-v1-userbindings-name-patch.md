+++
title = "差异化更新用户和角色的绑定关系信息"
description = "/apis/auth.alauda.io/v1/userbindings/{name} PATCH"
weight = 4
path = "PATCH /apis/auth.alauda.io/v1/userbindings/{name}"
+++


{{%api path="/apis/auth.alauda.io/v1/userbindings/{name}" verb="PATCH" %}}
