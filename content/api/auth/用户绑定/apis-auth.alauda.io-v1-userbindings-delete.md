+++
title = "批量解除用户和角色的绑定关系"
description = "/apis/auth.alauda.io/v1/userbindings DELETE"
weight = 7
path = "DELETE /apis/auth.alauda.io/v1/userbindings"
+++


{{%api path="/apis/auth.alauda.io/v1/userbindings" verb="DELETE" %}}
