+++
title = "解除指定的用户和角色的绑定关系"
description = "/apis/auth.alauda.io/v1/userbindings/{name} DELETE"
weight = 6
path = "DELETE /apis/auth.alauda.io/v1/userbindings/{name}"
+++


{{%api path="/apis/auth.alauda.io/v1/userbindings/{name}" verb="DELETE" %}}
