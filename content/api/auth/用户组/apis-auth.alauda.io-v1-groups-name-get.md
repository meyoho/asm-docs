+++
title = "查看用户组详情"
description = "/apis/auth.alauda.io/v1/groups/{name} GET"
weight = 3
path = "GET /apis/auth.alauda.io/v1/groups/{name}"
+++


{{%api path="/apis/auth.alauda.io/v1/groups/{name}" verb="GET" %}}
