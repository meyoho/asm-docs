+++
title = "查看用户信息"
description = "/apis/auth.alauda.io/v1/users/{name} GET"
weight = 3
path = "GET /apis/auth.alauda.io/v1/users/{name}"
+++


{{%api path="/apis/auth.alauda.io/v1/users/{name}" verb="GET" %}}
