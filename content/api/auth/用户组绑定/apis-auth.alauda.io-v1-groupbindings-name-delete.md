+++
title = "解除指定的用户和用户组的绑定关系"
description = "/apis/auth.alauda.io/v1/groupbindings/{name} DELETE"
weight = 6
path = "DELETE /apis/auth.alauda.io/v1/groupbindings/{name}"
+++


{{%api path="/apis/auth.alauda.io/v1/groupbindings/{name}" verb="DELETE" %}}
