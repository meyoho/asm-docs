+++
title = "查看用户和用户组的绑定关系详情"
description = "/apis/auth.alauda.io/v1/groupbindings/{name} GET"
weight = 3
path = "GET /apis/auth.alauda.io/v1/groupbindings/{name}"
+++


{{%api path="/apis/auth.alauda.io/v1/groupbindings/{name}" verb="GET" %}}
