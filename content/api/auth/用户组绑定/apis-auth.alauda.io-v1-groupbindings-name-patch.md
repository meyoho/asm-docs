+++
title = "差异化更新用户和用户组的绑定关系信息"
description = "/apis/auth.alauda.io/v1/groupbindings/{name} PATCH"
weight = 4
path = "PATCH /apis/auth.alauda.io/v1/groupbindings/{name}"
+++


{{%api path="/apis/auth.alauda.io/v1/groupbindings/{name}" verb="PATCH" %}}
