+++
title = "返回结果"
description = "请求发送以后，您会收到响应，包含状态码、响应消息头和消息体。"
weight = 3
+++

请求发送以后，您会收到响应，包含状态码、响应消息头和消息体。

状态码是一组从 1xx 到 5xx 的数字代码，状态码表示了请求响应的状态，完整的状态码列表请参见 [状态码]({{< relref "api/99prologue/01error-codes.md" >}})。

当接口调用成功，则会返回正常状态码及相应的返回体消息，不同请求的接口，返回体消息不同。

以 **删除用户** 接口为例，状态码为 200，返回体消息（Response Body）如下：

```json
{
    "kind": "Status",
    "apiVersion": "v1",
    "metadata": {
        
    },
    "status": "Success",
    "details": {
        "name": "89cd9f134faf20fdd49ff0205464b28a",
        "group": "auth.alauda.io",
        "kind": "users",
        "uid": "76d5ed98-be67-11e9-bf3f-52540006c2c3"
    }
}
```

当接口调用出错时，会返回错误状态码及错误信息说明。

以 **删除用户** 接口为例，状态码为 404，返回体消息如下：

```
{
    "kind": "Status",
    "apiVersion": "v1",
    "metadata": {
        
    },
    "status": "Failure",
    "message": "users.auth.alauda.io \"104@alauda.io\" not found",
    "reason": "NotFound",
    "details": {
        "name": "104@alauda.io",
        "group": "auth.alauda.io",
        "kind": "users"
    },
    "code": 404
}
```

可根据 `message ` 和 `reason` 的信息排查错误。