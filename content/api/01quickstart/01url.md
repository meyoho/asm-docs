+++
title = "API 调用方法"
description = "介绍 API 的服务使用方法、请求方法。"
weight = 1
+++

以下为您介绍 API 的服务使用方法、请求方法，并以示例说明如何调用 API。

## 服务使用方法

我们提供的 API 符合 REST API 的设计理论。

REST 从资源的角度来观察整个网络，分布在各处的资源由 URI（Uniform Resource Identifier）确定，而客户端的应用通过 URL（Unified Resource Locator）来获取资源。

URL 的一般组成结构为：

```
{URI-scheme}://{Endpoint}/{Resource-path}?{Query-param}
```
例如：
```
https://api-staging.alauda.cn/apis/auth.alauda.io/v1/projects?limit=20
```

URL 中的参数说明参见下表。


|  参数  |    是否必填项     |     说明      |
|  -------  |  ------- |  --------  |
| URI-scheme  |    是    | 用于传输请求的协议，当前所有 API 均采用 https 协议。<br>使用 https 协议表示通过安全的 SSL 加密的 HTTP 协议访问该资源。 |
| Endpoint  |   是    |  存放资源的服务器的域名或 IP，请获取实际部署的服务器的域名或 IP。例如：“api-staging.alauda.cn” 或 “192.144.193.251”。 |
| Resource-path  |      是  | 为资源路径，也即 API 的访问路径。<br>从每个接口的请求行及 URI 参数中获取，例如：查看项目详情接口的 URI 为“apis/auth.alauda.io/v1/projects”。 |
| Query-param  |   否  |    查询参数，接口请求时用于过滤、筛选的一些参数。是否必填请参见具体接口说明。<br>输入格式如：“q=value&q1=value1”。<br>**说明**：查询参数和请求行之间需要用 `?` 分隔。 |


## API 请求方法

在 HTTP 协议中，可以使用多种请求方法（也称为操作或动作），用于指明以何种方式来访问指定的资源。

| 方法 |  说明     |
|---------|---------|
| GET |  请求服务器返回指定资源。 |
| POST | 请求服务器新增资源或执行特殊操作。 |
| PUT |  请求服务器更新指定资源。 |
| DELETE | 请求服务器删除指定资源，如删除存储卷等。 |
| PATCH | 请求服务器更新资源的部分内容。<br>当资源不存在的时候，PATCH 可能会去创建一个新的资源。 |

在每个接口说明的请求行中，可查看具体的请求方法。例如：查看项目列表接口的请求行为 `GET apis/auth.alauda.io/v1/projects`，GET 即为当前接口的请求方法。


## 请求消息头

附加请求头字段，如指定的URI和HTTP方法所要求的字段。例如：定义消息体类型的请求头“Content-Type”，请求鉴权信息等。

| 名称 |  描述     |    是否必选  |
|---------|---------|-------|
|   Content-Type   | 发送的实体的 MIME 类型。推荐用户默认使用 `application/json`，如果 API 是对象、镜像上传等接口，媒体类型可按照流类型的不同进行确定。 | 是  |
|   Authorization   | 您的 Token。<br>API 调用时，会使用 Token 进行身份认证。需要再请求消息头中加入 Token 信息来标识发送请求的用户的身份。 | 是 |

## 请求消息体

即接口说明中的请求体（Request Body），该部分可选，GET、DELETE 操作类型的接口就不需要消息体，消息体具体内容需要根据具体接口而定。

请求消息体通常以结构化格式（如JSON或XML）发出，与请求消息头中 Content-Type 对应，传递除请求消息头之外的内容。若请求消息体中的参数支持中文，则中文字符必须为 UTF-8 编码。

## 字符编码格式

请求内容均需要使用 UTF-8 进行编码，否则我们将无法对一些特殊字符进行解码。

