+++
title = "Service Mesh"
description = ""
+++

# 欢迎使用 Service Mesh

Service Mesh 是基于容器和 Istio 框架的微服务治理平台，提供包括服务发现、服务监控、调用链追踪、流量管理、安全认证在内的全面微服务治理能力，在微服务之间建立安全、可靠、高效的通信。

使用本平台，有助于：

* 降低 Istio 框架部署、维护的成本。

* 减少开发对微服务框架的依赖，提高微服务治理的效率。

* 更好地应对配置管理复杂，服务间依赖关系复杂，服务的监控、鉴权、安全控制，远程调用等微服务架构带来的挑战。

![gailanshuoming](/img/gailanshuoming.png?classes=big)

## 用户手册

<ul class="children children-table">
   <span><span><a href="{{< relref "10usermanual/project/topology" >}}"> 服务拓扑</a></span></span>
   <span><span><a href="{{< relref "10usermanual/project/servicetable" >}}"> 服务列表</a></span></span>
   <span><span><a href="{{< relref "10usermanual/project/jaeger" >}}"> 调用链</a></span></span>
   <span><span><a href="{{< relref "10usermanual/project/istiogateway" >}}"> 服务网关</a></span></span>
   <span><span><a href="{{< relref "10usermanual/project/resourcelist" >}}"> 资源列表</a></span></span>
   <span><span><a href="{{< relref "10usermanual/admin/servicemesh" >}}"> 服务网格</a></span></span>
</ul>


## API 文档

<ul class="children children-table">
   <span><span><a href="{{< relref "api/user/microservices" >}}"> 服务列表</a></span></span>
   <span><span><a href="{{< relref "api/user/gateways" >}}"> 服务网关</a></span></span>
   <span><span><a href="{{< relref "api/admin/service_mesh_switch/clusterconfigs" >}}"> clusterconfig</a></span></span>
</ul>

## 常见问题

<ul class="children children-table">
   <span><span><a href="{{< relref "100faq/0" >}}"> 平台兼容的 PC 端浏览器有哪些</a></span></span>
   <span><span><a href="{{< relref "100faq/7" >}}"> 微服务治理功能出现问题时，如何排查问题</a></span></span>
</ul>
