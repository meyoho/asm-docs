+++
title = "服务列表"
description = ""
weight = 3
+++

服务列表展示了用户手动创建的微服务，并基于服务入口提供相关微服务治理功能，例如管理微服务的访问策略、访问权限和微服务路由。

**使用场景**

您可以在微服务治理平台上，在启用了 Service Mesh 的命名空间中，将您的业务作为微服务进行治理。

* 您需要首先在容器平台上创建运行业务的工作负载，然后在微服务治理平台，在同一命名空间中，创建与其关联的微服务，参考 [创建微服务]({{< relref "/10usermanual/project/servicetable/create.md" >}})。

	**说明**：
	
	* 目前支持 Deployment（部署）类型的工作负载。
	
	* 一个微服务可以关联多个工作负载，作为微服务的不同 **服务版本**。

* 创建微服务后，您可以在微服务详情页中，设置微服务的服务入口，参考 [管理服务入口]({{< relref "/10usermanual/project/servicetable/setgate/_index.md" >}})。

* 在微服务详情页中，查看服务入口信息和关联的微服务治理信息，参考 [查看微服务详情]({{< relref "/10usermanual/project/servicetable/view.md" >}})。

* 在微服务详情页中，支持管理服务入口的访问策略，参考 [管理安全策略]({{< relref "/10usermanual/project/servicetable/security/_index.md" >}})、[管理负载均衡策略]({{< relref "/10usermanual/project/servicetable/loadbalancer/_index.md" >}})、[管理熔断策略]({{< relref "/10usermanual/project/servicetable/hytrix/_index.md" >}})、[管理连接池设置策略]({{< relref "/10usermanual/project/servicetable/connectionpool/_index.md" >}})。

* 在微服务详情页中，支持管理服务入口的访问权限，提高服务访问的安全性。相关参数可参考 [管理访问控制]({{< relref "/10usermanual/project/servicetable/accesscontrol/_index.md" >}})。

* 在微服务详情页中，支持管理服务入口的微服务路由，可以对服务版本之间的流量分配进行附加控制，以实现灰度发布等发布策略。相关参数可参考 [管理微服务路由]({{< relref "/10usermanual/project/servicetable/route/_index.md" >}})、[管理路由策略]({{< relref "/10usermanual/project/servicetable/strategy.md" >}})。

* 如果要添加、更新或移除微服务的服务版本，参考 [管理服务版本]({{< relref "10usermanual/project/version/_index.md" >}})。

* 如果不再需要治理某个微服务，计划删除，参考 [删除微服务]({{< relref "10usermanual/project/servicetable/delete.md" >}})。

![servicetable](/img/servicetable/servicetable.png?classes=big)


