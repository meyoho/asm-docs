+++
title = "管理服务入口"
description = ""
weight = 2
+++

服务入口，即 Kubernetes 的 Service（内部路由）资源，您可在微服务治理平台上，基于服务入口创建访问策略、配置路由规则，进行微服务治理。每个微服务最多只能有一个服务入口。

服务入口分为以下两种类型：

* **容器平台创建的服务入口**：在您将工作负载与微服务关联之前，已经在容器平台中创建的与服务版本中的工作负载关联的内部路由，在工作负载与微服务关联成功后，自动成为微服务的服务入口。

* **微服务治理平台上创建的服务入口**：支持在微服务治理平台中，创建与微服务中的所有工作负载关联的内部路由，作为微服务的自定义服务入口，参考 [创建服务入口]({{< relref "10usermanual/project/servicetable/setgate/create.md" >}})。

	支持在微服务治理平台中更新或删除服务入口，参考 [更新服务入口]({{< relref "10usermanual/project/servicetable/setgate/update.md" >}})、[删除服务入口]({{< relref "10usermanual/project/servicetable/setgate/delete.md" >}})。
