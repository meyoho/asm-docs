+++
title = "更新服务入口"
description = ""
weight = 4
+++

更新服务入口。支持更新协议、端口信息。

**提示**：不支持更新服务入口的名称。

**操作步骤**

1. 登录平台，进入业务视图后，选择进入微服务所在的项目下命名空间。

2. 单击 **服务列表**，找到要更新服务入口的微服务。

3. 单击 ***微服务名称***，进入微服务详情页。

4. 单击页面下方的 **服务入口** 页签，再单击 **操作** > **更新**。

5. 在 **更新服务入口** 窗口中，更新服务入口的参数。

	* 在 **端口** 区域，可更新内部路由的访问策略，更新后需要保留至少一组端口映射。

		单击 ![](/img/addkey.png)，可添加一组端口映射，为 Kubernetes Service 配置集群内部服务端口和指向后端 Pod 的目标端口（targetPort）。<br>单击 ![](/img/cancelwhite.png)，可移除一组端口映射。
	
		* **协议**：选择内部路由（Kubernetes Service）的服务端口协议，支持 TCP、HTTP、HTTPS、HTTP2、gRPC。
	
		* **服务端口**：输入内部路由在集群内部，暴露关联工作负载的端口号（port）。
	
		* **容器端口**：输入容器对外暴露自身业务的端口号（targetPort），映射到对应的服务端口。

6. 单击 **更新**。