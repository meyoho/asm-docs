+++
title = "管理服务版本"
description = ""
weight = 2
+++

在微服务的工作负载变更、新版本上线、旧版本下线等情形下，如需更改关联的工作负载或版本标识，可以通过管理服务版本的系列功能进行操作。

支持 [添加服务版本]({{< relref "10usermanual/project/servicetable/version/add.md" >}})、[移除服务版本]({{< relref "10usermanual/project/servicetable/version/remove.md" >}}) 和 [更新服务版本标识]({{< relref "10usermanual/project/servicetable/version/update.md" >}})。
