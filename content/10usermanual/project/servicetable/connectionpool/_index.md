+++
title = "管理连接池设置策略"
description = ""
weight = 7
+++

连接池设置策略是可选的流量访问策略，使用适当的参数配置，可帮助您抵御流量负载过大的情况。  

目前平台支持对上游服务的 HTTP 请求数进行限制，当 HTTP 请求数过大时，可根据设置的策略参数，拒绝处理部分 HTTP 请求，达到控制网络峰值、辅助系统稳定运行的目的。支持 HTTP/1.1 和 HTTP/2 协议。