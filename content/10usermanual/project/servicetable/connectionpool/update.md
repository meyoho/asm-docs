+++
title = "更新连接池设置策略"
description = ""
weight = 3
+++

更新已创建的连接池设置策略。

**操作步骤**

1. 登录平台，进入业务视图后，选择进入微服务所在的项目下命名空间。

2. 单击 **服务列表**，找到要更新连接池设置策略的微服务。

3. 单击 ***微服务名称***，进入微服务详情页。

5. 单击页面下方的策略页签，找到要更新的连接池设置策略。

6. 在策略列表页面，单击 ![operations](/img/operations.png)，再单击 **更新**。

7. 在 **更新连接池设置** 窗口，更新参数配置。参考 [创建连接池设置策略（HTTP/HTTP2）]({{< relref "10usermanual/project/servicetable/connectionpool/createhttp.md" >}})。

8. 单击 **更新**。
