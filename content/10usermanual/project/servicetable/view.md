+++
title = "查看微服务详情"
description = ""
weight = 3
+++

查看微服务中的服务入口信息，微服务治理信息。

**操作步骤**

1. 登录平台，进入业务视图后，选择进入微服务所在的项目下命名空间。

2. 单击 **服务列表**，找到要查看详情的微服务。

3. 单击 ***微服务名称***，进入微服务详情页。

4. 在详情页左侧，可查看服务入口和服务版本之间的关系。

	* 下图表示服务版本没有服务入口。

	![nogate](/img/servicetable/nogate.png)
	
	* 下图表示当前服务入口对应的服务版本。

	![nowgate](/img/servicetable/nowgate.png)
	
	* 下图表示当前服务入口对应多个服务版本。

	![moregate](/img/servicetable/moregate.png)
	
	* ![lock](/img/servicetable/lock.png) 图标：表示影响服务入口及关联服务版本的安全策略已生效。

	* ![flash](/img/servicetable/flash.png) 图标：表示影响服务入口及关联服务版本的熔断策略已生效。
	
5. 在详情页右侧，可管理微服务的服务版本，参考 [管理服务版本]({{< relref "10usermanual/project/servicetable/version/_index.md" >}})。
	
6. 在详情页下方，可单击不同的页签，查看服务入口信息，以及服务入口访问策略、微服务路由、服务访问控制等微服务治理信息。

	![xiangqingye](/img/servicetable/xiangqingye.png?classes=big)

	* 单击 **服务入口** 页签，查看服务入口使用的协议和端口信息，参考 [管理服务入口]({{< relref "10usermanual/project/servicetable/setgate/_index.md" >}})。
		
		* **协议**：内部路由（Kubernetes Service）的服务端口协议。
		
		* **服务端口**：内部路由在集群内部，暴露关联工作负载的端口号（port）。

		* **容器端口**：容器对外暴露自身业务的端口号（targetPort），映射到对应的服务端口。

	* 单击 **策略** 页签，管理服务入口的访问策略，页签上的数字代表服务入口的访问策略数量。参考 [管理安全策略]({{< relref "10usermanual/project/servicetable/security/_index.md" >}})、[管理负载均衡策略]({{< relref "10usermanual/project/servicetable/loadbalancer/_index.md" >}})、[管理熔断策略]({{< relref "10usermanual/project/servicetable/hytrix/_index.md" >}})、[管理连接池设置策略]({{< relref "10usermanual/project/servicetable/connectionpool/_index.md" >}})。

	* 单击 **路由** 页签，管理服务入口的微服务路由，可以对服务版本之间的流量分配进行附加控制，以实现灰度发布等发布策略。相关参数可参考 [管理微服务路由]({{< relref "10usermanual/project/servicetable/route/_index.md" >}})、[管理路由策略]({{< relref "10usermanual/project/servicetable/strategy.md" >}})。
	
	* 单击 **访问控制** 页签，管理服务入口的访问权限，提高服务访问的安全性。相关参数可参考 [管理访问控制]({{< relref "10usermanual/project/servicetable/accesscontrol/_index.md" >}})。

