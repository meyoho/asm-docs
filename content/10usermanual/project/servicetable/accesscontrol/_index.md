+++
title = "管理访问控制"
description = ""
weight = 8
+++

通过为服务入口进行访问控制，为安全级别较高、数据较敏感的服务提供权限控制，防止服务之间的随意调用，优化微服务调用关系架构。

目前支持通过管理访问白名单，实现基础的访问权限控制。

* 在未添加任何白名单成员时，外部服务的调用访问不受限制。
	
* 在添加白名单后，只有当服务符合白名单规则时，允许其请求调用，否则不可调用。

