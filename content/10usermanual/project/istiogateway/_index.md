+++
title = "服务网关"
description = "网关管理主要负责微服务外部对微服务的访问流量管理。"
weight = 5
+++

Gateway 为 HTTP 流量配置了一个负载均衡，多数情况下在网格边缘进行操作，用于启用一个服务的入口（ingress）流量。

Istio Gateway 只配置 HTTP 四层到六层的功能，例如开放端口或者 TLS 配置。绑定一个 VirtualService 到 Gateway 上，就可以使用标准的 Istio 规则来控制进入的 HTTP 流量。

网关管理主要负责微服务外部对微服务的访问流量管理。

