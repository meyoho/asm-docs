+++
title = "管理网关路由"
description = ""
weight = 3
+++

网关路由主要负责微服务外部的访问流量管理，借助该功能，可实现 A/B 测试、灰度发布等场景。

对网关配置路由规则，可以根据服务版本对流量进行控制。路由规则包括：

* 按照指标配置的 **条件规则**；

* 按照权重方式配置的 **权重规则**。

支持针对匹配路由规则的流量，进一步设置路由策略，实现高级流量管理。支持的路由策略包括：

* 错误注入（Fault Injection）

* 延迟返回

* 超时和重试

* 请求重写

* 流量复制

