+++
title = "创建网关路由"
description = ""
weight = 1
+++

创建网关路由，通过设定路由规则，主要负责微服务外部的访问流量管理，根据服务版本对流量进行附加控制。

**提示**：通过设置权重规则，可实现服务的灰度发布，从而使业务做到平滑过渡。

**前提条件**

1. 在微服务中，已存在使用 HTTP 或 HTTP2 协议的服务入口。

**操作步骤**

1. 登录平台，进入业务视图后，选择进入服务网关所在的项目下命名空间。

2. 单击 **服务网关**，找到要创建路由的服务网关。

3. 单击 ***网关名称***，进入微服务详情页。

4. 单击页面下方的 **网关路由** 页签，单击 **创建路由**。

5. 在 **创建路由** 窗口，在 **名称** 框中，输入路由的名称。   
  名称支持小写英文字母、数字 0 ~ 9、中横线、点 (.)。字符数大于等于 1 个，小于等于 32 个。不支持以中横线开头或结尾。
  
6. （非必填）在 **显示名称** 框中，输入路由的显示名称，支持中文字符。如未输入，默认显示为空。

7. （非必填）在 **条件规则** 区域，单击 **添加条件规则组**，可开始设置条件规则；如存在多组条件规则，按照先后顺序确定优先级。参见 [条件规则说明]({{< relref "10usermanual/project/rulea.md" >}})。

8. 在 **权重规则** 区域，选择一个或多个工作负载后，设置权重的百分比。参见 [权重规则说明]({{< relref "10usermanual/project/ruleb.md" >}})。

9. 单击 **创建**。