+++
title = "查看网关路由详情"
description = ""
weight = 2
+++

查看网关路由的基本信息、路由规则和路由策略。

**操作步骤**

1. 登录平台，进入业务视图后，选择进入服务网关所在的项目下命名空间。

2. 单击 **服务网关**，找到要查看路由详情的服务网关。

3. 单击 ***网关名称***，进入网关详情页。

4. 单击页面下方的 **网关路由** 页签。
	
5. 查看路由的基本信息，部分参数说明如下。

	* **状态**：路由是否启用，包括 **已启用** 和 **未启用** 两种状态。

	* **生效状态**：路由是否有效，包括 **已生效** 和 **未生效**两种状态。

6. 查看路由规则信息和路由策略，参见 [条件规则说明]({{< relref "10usermanual/project/rulea.md" >}})、[权重规则说明]({{< relref "10usermanual/project/ruleb.md" >}})、[管理网关路由策略]({{< relref "10usermanual/project/istiogateway/strategy.md" >}})。