+++
title = "调用链"
description = "通过 Jaeger 展示项目中微服务之间的完整调用链，例如通过设置的搜索条件，查看调用链详情。通过调用链跟踪系统，可以快速了解各节点的响应状况，方便定位问题。"
weight = 2
+++

通过 Jaeger 展示项目中微服务之间的完整调用链，例如通过设置的搜索条件，查看调用链详情。通过调用链跟踪系统，可以快速了解各节点的响应状况。

**Jaeger 架构**

Jaeger 是一种分布式调用链跟踪系统，主要用于多个服务调用过程追踪分析，图形化服务调用轨迹，便于快速准确定位问题。

![jaegerarchitecture](/img/jaegerarchitecture.png?classes=big)

Jaeger 主要由以下几部分组成：

* Jaeger Client：为不同语言实现了符合 OpenTracing 标准的 SDK。应用程序通过 API 写入数据，client library 把 trace 信息按照应用程序指定的采样策略传递给 jaeger-agent。

* Agent：它是一个监听在 UDP 端口上接收 span 数据的网络守护进程，它会将数据批量发送给 collector。它被设计成一个基础组件，部署到所有的宿主机上。Agent 将 client library 和 collector 解耦，为 client library 屏蔽了路由和发现 collector 的细节。

* Collector：接收 jaeger-agent 发送来的数据，然后将数据写入后端存储。Collector 被设计成无状态的组件，因此可以同时运行任意数量的 jaeger-collector。

* Data Store：后端存储被设计成一个可插拔的组件，支持将数据写入 Cassandra、Elasticsearch。

* Query：接收查询请求，然后从后端存储系统中检索 trace 并通过 UI 进行展示。Query 是无状态的，可以启动多个实例，把它们部署在 Nginx 这样的负载均衡器后面。

**操作步骤**

1. 登录平台，进入业务视图后，选择要进入的项目下命名空间。

2. 单击左导航栏中的 **调用链**。

2. 在调用链页面，查看 Jaeger 集成到微服务的调用追踪，定位问题。此调用链页面基于 Jaeger 原生页面做了修改，显示了服务之间的调用流，并通过添加的搜索条件，查看调用链详情。设置查询条件，查看服务的调用追踪详情。

	* 服务：当前项目下所有 Kubernetes 服务。
		
	* 操作：要查询的服务操作。
		
	* 标签（Tags）：一组键值对构成的 Span 标签集合。键值对中，键的格式是 string。值可以是 string、boolean、或数字类型，格式为 logfmt。当有多个值时，使用空格来间隔。如果值包含空格，空格使用引号（"）括起来。例如：`error=true db.statement="select * from User"`。
		
	* 时间范围（Lookbacks）：查询的时间范围，支持自定义。 
		
	* 最小调用时间（Min Duration）和最大调用时间（Max Duration）：通过设定两个相邻时间的最小调用时间和最大调用时间，得到具体的调用时间范围，这个范围的时间大于最小调用时间，小于最大调用时间。例如：最小时间设置了 5 秒，最大时间设置了 15 秒，查询出总调用时间大于 5 秒，并小于 15 秒的所有调用链。

	* 最多显示数量（Limit Results）：查询的最多显示结果，即最多显示多少条 trace 结果。

3. 单击 **搜索**，查看服务的调用追踪详情。支持用多种不同视图对追踪结果进行可视化，例如追踪时段内的直方图，或服务在追踪过程中的累积时间，并提供了数据的向下挖掘能力。

	![jeagerchain1](/img/jeagerchain1.png?classes=big)

	* 显示时间和持续时间散点图：点击散点图中的点，调转到调用链详情页面，查看服务的调用结果，例如 HTTP 的 URL，调用方式，返回码等。
  
		![jaegerdetail](/img/jaegerdetail.png?classes=big)
	
	* 查询结果中的调用链：选择任意两个查询结果，单击 **Compare Traces**，查看对比结果。

	更多信息，参考 Jaeger 和 Istio 官方文档。





