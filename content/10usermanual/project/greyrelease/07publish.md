+++
title = "执行发布"
description = ""
weight = 7
+++

触发执行灰度发布。当灰度发布初始化完成且未处在发布中状态时，可执行发布。

<style>ol ol { list-style-type: lower-roman;}</style>

**操作步骤**

1. 登录平台，进入业务视图后，选择要进入的项目下命名空间。

2. 单击左导航栏中的 **灰度发布**。

3. 在灰度发布列表页面，单击要执行发布的 ***灰度发布名称***，在灰度发布详情页面，单击 **操作** > **发布**。

4. 在发布窗口，用户可编辑修改 YAML。

5. 单击 **发布**。执行发布后跳转发布记录详情页。    