+++
title = "查看灰度发布"
description = ""
weight = 2
+++

查看灰度发布详情。

**操作步骤**

1. 登录平台，进入业务视图后，选择要进入的项目下命名空间。

2. 单击左导航栏中的 **灰度发布**。

3. 在灰度发布列表页面，单击要查看的 ***灰度发布名称***。

    ![greyrelease2](/img/greyrelease2.png?classes=big)

4. 在详情页面，在 **服务版本** 区域，查看最近一次发布的服务版本信息。

	* **执行 ID**：最近一次发布记录的 ID。单击 ***执行 ID***，查看发布记录的详情，参考 [查看发布记录]({{< relref "/10usermanual/project/greyrelease/09record.md" >}})。

	* **状态**：最近一次发布的状态；包括发布中，发布成功，发布失败。单击状态后的异常日志入口，弹出异常日志弹框，展示发布过程中产生的 warning 级别的日志。

	* **金丝雀版本** 和 **主版本**：创建灰度规则时，将复制原服务版本（金丝雀版本）配置，创建出后缀为 primary 的服务版本（主版本），同时流量将全部切换至主版本，金丝雀版本实例数调度为 0。  
	通过更新金丝雀版本配置触发灰度发布，灰度发布时，调度金丝雀版本实例，并按照发布规则将流量切换至配置更新后的金丝雀版本。发布完成后，将金丝雀配置复制到主版本，金丝雀实例重新调度为 0，由主版本提供最新服务。
	金丝雀版本的实例数不可手动扩缩容。
	
5. 在 **发布规则** 区域，查看灰度发布的具体规则，参考 [创建灰度发布]({{< relref "/10usermanual/project/greyrelease/01create.md" >}})。

6. 在 **发布记录** 区域，查看灰度发布的历次发布记录。单击 ***执行 ID***，查看发布记录的详情，参考 [查看发布记录]({{< relref "/10usermanual/project/greyrelease/09record.md" >}})。