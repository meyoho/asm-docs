+++
title = "为用户添加角色"
description = "为指定的用户添加角色。"
weight = 5
+++

为指定的用户添加角色，使用户拥有相应角色的权限。

**说明**：仅平台管理员角色的用户具有为其他用户添加角色的权限。

**操作步骤**

1. 使用平台管理员账号登录平台后，单击页面右上方的 ***账号名称*** > **平台管理**，打开平台管理页面。

2. 在左侧导航栏中单击  **用户管理** 选项，展开用户列表。

3. 单击指定的用户记录右侧的 ![](/img/003point.png) 图标，在展开的下拉列表中单击 **管理角色**。

4. 在弹出的 **管理角色** 对话框中，单击 **角色** 右侧的下拉选择框并选择要添加的角色。

	**说明**：
	
	* 相同的角色只能为同一用户添加一次，用户已有的角色在下拉选择框中显示不可选；
	
	* 选中角色后，会根据角色的实际工作空间进行判断，并在 **管理角色** 对话框中角色的右侧展现项目或项目和命名空间下拉选择框。
	
5. 在出现的 **项目** 或 **项目** 和 **命名空间** 下拉选择框中选择相应的项目或命名空间。

6. 单击 **添加** 按钮，即可为用户添加相应的角色。
