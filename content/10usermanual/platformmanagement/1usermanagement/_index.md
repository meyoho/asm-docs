+++
title = "用户管理"
description = "为用户添加或删除角色。"
weight = 1
alwaysopen = false
+++


平台允许每个人以用户的身份登录平台，并对登录信息进行验证。平台的用户有以下几种来源：

* LDAP：通过同步 LDAP（Lightweight Directory Access Protocol，轻量级目录访问协议）导入企业已有用户账号。<br>可通过 IDP 配置。


* OIDC：平台认可的通过 OIDC（OpenId Connect）协议登录平台的第三方平台的用户账号。<br>可通过 IDP 配置。


* Local：平台部署时自动创建的管理员账号或通过修改本地 dex 配置文件创建的账号。<br>如果需要添加本地用户，请联系运维工程师获得支持。

* 其他第三方平台用户：平台支持的 dex 已实现的连接器（Connectors）认证的账号，例如：GitHub、Microsoft 等。<br>如需了解更多，请参考 [dex 官方文档](https://github.com/dexidp/dex)。

平台管理员通过为用户添加角色或删除用户已有的角色，可添加或收回用户的操作权限。同时，平台管理员还具有在平台上同步 LDAP 用户和本地配置文件中添加的用户、清理平台上的无效用户的权限。

{{%children style="card" description="true" %}}
