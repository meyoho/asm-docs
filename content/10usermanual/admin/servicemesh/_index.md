+++
title = "服务网格"
description = ""
weight = 2
alwaysopen = false
+++

在您开始体验 Service Mesh 的各项微服务治理功能之前，管理员需要首先为每一个需要微服务治理的 Kubernetes 集群，部署包括 Istio 服务网格管理平台在内的所有 Service Mesh 组件，并接入必要的外部组件，使集群具备容器化微服务治理的能力。

服务网格管理功能将部署 Service Mesh 组件流程可视化。管理员只需配置主要参数，可以方便快捷的执行部署服务网格、修改服务网格参数等操作。

* 管理员在平台管理的 Kubernetes 集群上，部署一套完整的 Service Mesh 组件，接入必要的外部组件，参考 [创建服务网格]({{< relref "/10usermanual/admin/servicemesh/01install.md" >}})。

* 在服务网格详情页中，查看服务网格的基本信息和参数配置，参考 [查看服务网格详情]({{< relref "/10usermanual/admin/servicemesh/02view.md" >}})。

* 如果根据实际情况，在创建服务网格后需要调整 Istio 的参数配置，或接入组件的对接参数，参考 [更新服务网格]({{< relref "/10usermanual/admin/servicemesh/03update.md" >}})。

* 如果集群不再需要微服务治理，计划卸载 Service Mesh 相关组件，参考 [删除服务网格]({{< relref "/10usermanual/admin/servicemesh/04uninstall.md" >}})。