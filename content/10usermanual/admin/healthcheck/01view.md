+++
title = "查看健康检查结果"
description = ""
weight = 1
+++
  
后台检查程序可定时运行并刷新健康状态，您也可以手动触发健康状态检查。

**操作步骤**

1. 登录平台，进入管理视图后，单击左导航栏中的 **服务网格**。

2. 在服务网格列表页，单击要查看健康状态的 ***服务网格名称***。

3. 在服务网格详情页，单击 **健康检查** 页签。

3. 在 **组件检测** 区域，查看组件健康状态。支持检查的组件包括核心组件和外部组件两类，参见[支持健康检查的组件]({{< relref "/10usermanual/admin/healthcheck/02componentsupport.md" >}})。

	* 组件状态标记为绿色，表示组件健康；组件状态为红色，表示组件不健康，将鼠标移动到相应的组件上，查看错误信息。

		![check](/img/healthcheck/check.png)
	
	* 单击右上角的刷新按钮，可启动检查程序，刷新组件状态。刷新按钮左侧的时间为上次刷新组件健康状态的时间。

		![refresh](/img/healthcheck/refresh.png)
	
	* 下图所示的图标表示系统正在执行检查程序，检查完成后将自动刷新组件健康状态。

		![running](/img/healthcheck/running.png)

4. 在 **功能检测** 区域，查看功能检查状态。检查覆盖重要的系统功能，参见[支持健康检查的功能]({{< relref "/10usermanual/admin/healthcheck/03functionsupport.md" >}})。

	* 在功能卡片上，查看每个功能的检测项列表。检测项状态标记为绿色，表示检测项健康；检测项状态为红色，表示检测项不健康，将鼠标移动到相应的检测项上，查看错误信息。  
	
		单击右上角的刷新按钮，可启动检查程序，刷新检测项状态。刷新按钮左侧的时间为上次刷新检测项健康状态的时间。

		![functioncheck](/img/healthcheck/functioncheck.png)
	
	* 下图所示的图标表示系统正在执行检查程序，检查完成后将自动刷新检测项健康状态。

		![running](/img/healthcheck/running.png)
