+++
title = "健康检查"
description = ""
weight = 5
+++

平台为集群内的 Service Mesh 服务组件，以及重要系统功能提供健康状态的检查。

**使用场景**

平台的后台检查程序可定时运行并刷新健康状态，您也可以手动触发健康状态检查。

* 通过查看组件和功能的健康检查结果，管理员能够及时了解系统运行状态，辅助定位问题，参考[查看健康检查结果]({{< relref "/10usermanual/admin/healthcheck/01view.md" >}})。

* 了解健康检查功能的覆盖范围，参考[支持健康检查的组件]({{< relref "/10usermanual/admin/healthcheck/02componentsupport.md" >}}) 和 [支持健康检查的功能]({{< relref "/10usermanual/admin/healthcheck/03functionsupport.md" >}})。
