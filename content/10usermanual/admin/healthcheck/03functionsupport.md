+++
title = "支持健康检查的功能"
description = ""
weight = 3
+++
	
* **SideCar 注入**：检测项包括 istio-sidecar-injector 和 istio-citadel 组件状态。

* **监控**：检测项包括 prometheus 访问状态、grafana 组件状态和 serviceMonitor 状态。