+++
title = "支持健康检查的组件"
description = ""
weight = 2
+++

<style>table th:first-of-type { width: 80px;}</style> 

<style>table th:nth-of-type(2) { width: 300px;}</style>


<table>
  <tr>
    <th>组件类型</th>
    <th>组件名称</th>
  </tr>
  <tr>
    <td rowspan="9">核心组件</td>
    <td>istio-pilot</td>
  </tr>
  <tr>
    <td>istio-sidecar-injector</td>
  </tr>
  <tr>
    <td>istio-citadel</td>
  </tr>
  <tr>
    <td>istio-policy</td>
  </tr>
  <tr>
    <td>istio-telemetry</td>
  </tr>
  <tr>
    <td>istio-gallery</td>
  </tr>
  <tr>
    <td>istio-ingressgateway</td>
  </tr>
  <tr>
    <td>istio-egressgateway</td>
  </tr>
  <tr>
    <td>asm-controller</td>
  </tr>
  <tr>
    <td rowspan="7">外部组件</td>
    <td>Grafana</td>
  </tr>
  <tr>
    <td>Jaeger-operator</td>
  </tr>
  <tr>
    <td>Jaeger-prod-collector</td>
  </tr>
  <tr>
    <td>Jaeger-prod-es-index-cleaner</td>
  </tr>
  <tr>
    <td>Jaeger-prod-query</td>
  </tr>
  <tr>
    <td>Elasticsearch（访问状态）</td>
  </tr>
  <tr>
    <td>Prometheus（访问状态）</td>
  </tr>
</table>