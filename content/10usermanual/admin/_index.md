+++
title = "管理视图（Alpha）"
description = ""
weight = 4
alwaysopen = false
+++

管理视图主要面向平台管理员，专注于平台级的管理，例如服务网格管理，Istio 监控，健康检查，在项目下命名空间启停 Service Mesh等。

* [项目]({{< relref "/10usermanual/admin/project/_index.md" >}})

* [监控]({{< relref "/10usermanual/admin/istiomonitor.md" >}})

* [服务网格]({{< relref "/10usermanual/admin/servicemesh/_index.md" >}})

* [健康检查]({{< relref "/10usermanual/admin/healthcheck/_index.md" >}})

