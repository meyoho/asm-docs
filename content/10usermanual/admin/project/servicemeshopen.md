+++
title = "启用 Service Mesh"
description = ""
weight = 2
+++

启用命名空间的 Service Mesh 功能，进行微服务治理。启用后，应用下的 Pod 发布时会被自动注入 Sidecar 容器，Sidecar 将会代理该 Pod 和其他服务间的通信。同时我们会提供服务拓扑、调用链路追踪、流量管理等功能。
命名空间的 Service Mesh 默认处于关闭状态。

**操作步骤**

1. 登录平台，进入管理视图后，单击左导航栏中的 **项目**。

2. 在项目列表页，单击要启用 Service Mesh 的命名空间所在的 ***项目名称***。

3. 在项目详情页面的命名空间区域，找到要启用 Service Mesh 的命名空间。

4. 单击命名空间对应的 ![operations](/img/operations.png)，再单击 **启用 Service Mesh**。

5. 在新窗口中，单击 **启用**，使项目中应用下的 Pod 发布时会被自动注入 Sidecar 容器，Sidecar 将会代理该 Pod 和其他服务间的通信。同时我们会提供服务拓扑、调用链路追踪、流量管理等功能。    
已发布的应用手动重启后，应用下的 Pod 自动注入 Sidecar 容器。

![servicemeshopen](/img/servicemeshopen.png?classes=big)