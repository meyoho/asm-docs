+++
title = "停用 Service Mesh"
description = ""
weight = 3
+++

停用 Service Mesh 后，命名空间中已发布的应用经手动重启后，平台会自动清除其 Pod 中注入的 Sidecar 容器，项目中的服务不再由 Service Mesh 治理。

**操作步骤**

1. 登录平台，进入管理视图后，单击左导航栏中的 **项目**。

2. 在项目列表页，单击要停用 Service Mesh 的命名空间所在的 ***项目名称***。

3. 在项目详情页面的命名空间区域，找到要停用 Service Mesh 的命名空间。

4. 单击命名空间对应的 ![operations](/img/operations.png)，再单击 **停用 Service Mesh**。  
停用后，项目下已发布的应用手动重启后，平台会自动清除其 Pod 中注入的 Sidecar 容器，项目中的服务不再由 Service Mesh 治理。

5. 在新窗口中，单击 **停用**。