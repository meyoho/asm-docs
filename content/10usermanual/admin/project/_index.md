+++
title = "项目"
description = ""
weight = 1
+++

针对项目下命名空间，进行 Service Mesh 相关配置。

* 启用或停用 Service Mesh；

* 设置命名空间的可见性。