+++
title = "监控"
description = "嵌入 Istio Grafana 仪表板来监控 Istio 的整体运行状态。"
weight = 3
+++

嵌入 Istio Grafana 仪表板来监控 Istio 的整体运行状态。服务网格的状态为 **成功** 时方可查看。

**操作步骤**

1. 登录平台，进入管理视图后，单击左导航栏中的 **服务网格**。

2. 在服务网格列表页，单击要查看监控的 ***服务网格名称***。

3. 在服务网格详情页，单击 **监控** 页签。

4. 在监控页面，查看 Istio Grafana 仪表板的默认 Dashboard（Istio Default Dashboard），包括常用的 Mesh 信息和性能 Performance 信息。

	![grafanaboard](/img/grafanaboard.png?classes=big)

	- **常用 Mesh 信息**：Global Request Volume、Gloabal Success Rate、4xxs、5xxs、Virtual Services、Destination Rules、Gateways、Authentication Mesh Policies；
	
	- **性能 Performance 信息**：vCPU Usage、Memory and Data Rates、Proxy Resource Usage、Pilot Resource Usage、Mixer Resource Usage、Galley Resource Usage、Citadel Resource Usage。

5. 支持选择切换查看其它预置的 Dashboard。 
	
	* Istio Galley Dashboard
		
	* Istio Mesh Dashboard
		
	* Istio Performance Dashboard
		
	* Istio Service Dashboard

	* Istio Citadel Dashboard
		
	* Istio Workload Dashboard
		
	* Istio Mixer Dashboard
		
	* Istio Pilot Dashboard

	* Controller Dashboard
	
	更多信息，参考 [Istio 文档](https://istio.io/docs/tasks/telemetry/metrics/using-istio-dashboard)。 