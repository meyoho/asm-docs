+++
title = "联邦命名空间管理（Alpha）"
description = "当项目关联的集群中包含联邦集群时，支持在该项目下创建联邦命名空间。创建联邦命名空间时，会在联邦集群所有的成员集群下都创建一个同名的命名空间。"
weight = 4
alwaysopen = false
+++

当项目关联的集群中包含联邦集群时，支持在该项目下创建联邦命名空间。创建联邦命名空间时，会在联邦集群所有的成员集群下都创建一个同名的命名空间。

通过联邦命名空间，可以创建和管理联邦应用。

支持在控制集群下的联邦命名空间中创建联邦应用及应用下的内部路由、访问规则、配置等联邦资源，并可在非控制集群的联邦成员集群的联邦命名空间下自动创建同名联邦资源。


<img src="/img/fedcluster.png" width = "460" />

平台管理员或项目管理员可创建、查看、更新、删除联邦命名空间。

{{%children style="card" description="true" %}}