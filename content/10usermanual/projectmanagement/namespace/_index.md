+++
title = "命名空间管理"
description = "在项目关联的集群中创建新的命名空间（Namespace），将创建的应用、容器组（Pod）等分配到不同的命名空间中，便于资源分组管理。"
weight = 3
alwaysopen = false
+++

在项目关联的集群中创建新的命名空间（Namespace），将创建的应用、容器组（Pod）等分配到不同的命名空间中，便于资源分组管理。


<img src="/img/00ns.png" width = "860" />

平台管理员或项目管理员可创建、查看、更新、删除命名空间，并管理命名空间成员。

{{%children style="card" description="true" %}}